﻿using System;

namespace quickFib
{
    //Обявили структуру
    struct Mtx2x2
    {
        public int _11, _12, _21, _22;
        //Перегрузили * для этой структуры
        public static Mtx2x2 operator *(Mtx2x2 lhs, Mtx2x2 rhs)
        {
            return new Mtx2x2
            {
                _11 = lhs._11 * rhs._11 + lhs._12 * rhs._21,
                _12 = lhs._11 * rhs._12 + lhs._12 * rhs._22,
                _21 = lhs._21 * rhs._11 + lhs._22 * rhs._21,
                _22 = lhs._21 * rhs._12 + lhs._22 * rhs._22
            };
        }
    }

    class Program
    {
        private static readonly Mtx2x2 fibMtx = new() { _11 = 1, _21 = 1,
                                                        _12 = 1, _22 = 0};
        private static readonly Mtx2x2 identity = new() { _11 = 1, _22 = 1 };
        static void Main(string[] args)
        {
            for (int i = 1; i < 40; i++)
                Console.WriteLine(Fibm(i));

            for (int i = 1; i < 40; i++)
                Console.WriteLine(Fibr(i));
        }       
        static int Fibm(int n)
        {
            return WhilePow(fibMtx, (n - 1))._11;
        }
        static int Fibr(int n)
        {
            return RecPow(fibMtx, (n - 1))._11;
        }
        static Mtx2x2 WhilePow(Mtx2x2 matrix, int exponent)
        {
            if (exponent == 0) return identity;
            if (exponent == 1) return matrix;
            var res = matrix;
            var tail = identity;
            while (exponent > 1)
            {
                if (exponent % 2 == 1)
                    tail *= res;
                res *= res;
                exponent /= 2;
            }
            return res * tail;
        }
        static Mtx2x2 RecPow(Mtx2x2 matrix, int exponent)
        {
            if (exponent == 0) return identity;
            if (exponent == 1) return matrix;

            var npdiv2 = RecPow(matrix, exponent / 2);
            return npdiv2 * npdiv2 * RecPow(matrix, exponent % 2);
        }
    }
}
